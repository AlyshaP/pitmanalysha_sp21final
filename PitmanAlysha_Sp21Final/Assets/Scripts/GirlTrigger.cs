﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GirlTrigger : MonoBehaviour
{
    public List<GameObject> Colliders;

    // Start is called before the first frame update
    void Start()
    {
        Colliders = new List<GameObject>();   
    }

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i<Colliders.Count; i++)
        {
            if (Colliders[i].activeInHierarchy == false)
            {
                Colliders.RemoveAt(i);
                i--;
            }
        }
        if (Colliders.Count == 0)
        {
            GameOver();
        }
    }
    void OnTriggerEnter(Collider other)
    {
        print("enter:" + other.gameObject.name);

        if (other.gameObject.tag == "LightCollider") //if she walks into the collider tagged "lightcollider"
        {
            if(Colliders.Contains(other.gameObject) == false) //if she is not within the collider
            {
                Colliders.Add(other.gameObject); //make her in the collider??
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        print("exit:" + other.gameObject.name);

        if (other.gameObject.tag == "LightCollider") //referencing objects with "lightcollider" tag
        {
            if (Colliders.Contains(other.gameObject) == true) //if she enters the collider
            {
                Colliders.Remove(other.gameObject); 
                if(Colliders.Count == 0) //if the collider does not exist (anymore)
                {
                    GameOver(); //game over
                }
            }
        }
    }
    void GameOver() //during game over
    {
        Destroy(gameObject); //destroy her
                             // specify fail condition
    }
}
