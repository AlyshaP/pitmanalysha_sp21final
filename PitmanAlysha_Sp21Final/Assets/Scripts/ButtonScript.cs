﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonScript : MonoBehaviour
{
    public GameObject lightParent;
    private float timer;

    public void Interact() //if the trigger collider is clicked
    {
        lightParent.SetActive(true); //turn on the linked light parent which will activate its collider
        timer = 10f; //for 10 seconds
        //there's nothing specifiying this can only be done once, so it's already set up to restart the timer if clicked again
    }
    void Start()
    {
        
    }

    public void Update()
    {
        if (timer > 0f) //if the timer runs out
        {
            timer -= Time.deltaTime;
            print(timer); //print timer status
            if (timer < 0f) //if the timer has been running longer than the allotted 10 seconds
            {
                lightParent.SetActive(false); //turn off the light parent
            }
        }
    }
}
