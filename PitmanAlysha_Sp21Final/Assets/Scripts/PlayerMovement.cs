﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController controller;

    public float speed = 12f;
    public float gravity = -9.81f;
    public float jumpHeight = 3f;

    public Transform groundCheck;
    public float groundDistance = 0.4f;
    public LayerMask groundMask;

    public float reachDistance = 5f; //refer back to, public- find in Editor
    public LayerMask interactMask;

    Vector3 velocity;
    bool isGrounded;

    // Update is called once per frame
    void Update()
    {
        DetectClick();
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);

        if(isGrounded && velocity.y < 0)
        {
            velocity.y = -2f;
        }

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;

        controller.Move(move * speed * Time.deltaTime);

        if(Input.GetButtonDown("Jump") && isGrounded)
        {
            velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
        }

        velocity.y += gravity * Time.deltaTime;

        controller.Move(velocity * Time.deltaTime);
    }
    void DetectClick()
    {
        if(Input.GetButtonDown("Fire1")) //Unity>edit>projectsettings>inputmanager>axes> 
            //tells you what buttons are associated with what controls. "Fire1" is Unity's left mouse click
        {
            RaycastHit hit; //sends a raycast out from the camera 
            if(Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, reachDistance, interactMask)) 
                //"out hit" makes it so what the ray cast hits will return with updated information
            {
                if(hit.collider.tag == "Button") //if the raycast hits a collider tagged "button"
                {
                    hit.collider.GetComponent<ButtonScript>().Interact(); //activate or interact with that object's script
                }
            }
        }
    }
}
