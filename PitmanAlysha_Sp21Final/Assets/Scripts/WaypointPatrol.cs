﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WaypointPatrol : MonoBehaviour
{
    public NavMeshAgent navMeshAgent;
    public Transform[] waypoints;

    public int m_CurrentWaypointIndex;

    void Start()
    {
        navMeshAgent.SetDestination(waypoints[0].position); //when the game starts, have her be at the first waypoint
    }

    void Update()
    {
        if (navMeshAgent.remainingDistance < navMeshAgent.stoppingDistance)
        {
             if(m_CurrentWaypointIndex+1 == waypoints.Length) //if her next stop is beyond a first run through of the waypoint index
            {
                print("You Win"); //specifies win condition 
            }
             else
            {
                m_CurrentWaypointIndex = (m_CurrentWaypointIndex + 1); //if she isn't done with her waypoint index
                navMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position); //move on to the next waypoint in line
            }
        }
    if (waypoints[m_CurrentWaypointIndex].gameObject.activeInHierarchy == false) //if 
        {
            navMeshAgent.isStopped = true; //she stops
        }
    else
        {
            navMeshAgent.isStopped = false; //she continues
        }
    }
}
